package cz.gopas.kalkulacka;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity
		extends AppCompatActivity
		implements CalcFragment.AboutShower, ConstFragment.ConstListener {

	private static final String TAG = MainActivity.class.getSimpleName();

	@BindView(R.id.bottom)
	BottomNavigationView bottom;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Toast.makeText(this, "Create", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "Create");

		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, new CalcFragment())
					.commit();
		}

		ButterKnife.bind(this);
		bottom.setOnNavigationItemSelectedListener(item -> {
			Log.d(TAG, String.valueOf(item.getItemId()));
			switch (item.getItemId()) {
				case R.id.about:
					getSupportFragmentManager().beginTransaction()
							.replace(R.id.container, new AboutFragment())
							.commit();
					return true;
				case R.id.calc:
					getSupportFragmentManager().beginTransaction()
							.replace(R.id.container, new CalcFragment())
							.commit();
					return true;
				default:
					return false;
			}
		});

		try {
			Log.d(TAG, getIntent().getDataString());
		} catch (Exception e) {
		}

		FragmentManager.enableDebugLogging(true);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Toast.makeText(this, "Resume", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "Resume");
	}

	@Override
	protected void onPause() {
		Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "Pause");
		super.onPause();
	}

	@Override
	public void showAbout() {
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.container, new AboutFragment())
				.addToBackStack(null)
				.commit();
		bottom.getMenu().findItem(R.id.about).setChecked(true);
	}

	@Override
	public void showConstants() {
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.container, new ConstFragment())
				.addToBackStack(null)
				.commit();
	}

	@Override
	public void showConstant(float constant) {
	}

	/*
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("RES", res.getText().toString());
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		res.setText(savedInstanceState.getString("RES"));
	}
	*/
}
