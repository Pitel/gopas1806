package cz.gopas.kalkulacka;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ConstListener} interface
 * to handle interaction events.
 */
public class ConstFragment extends Fragment {

	private ConstListener mListener;

	public ConstFragment() {
		// Required empty public constructor
	}

	@BindView(R.id.list)
	RecyclerView list;

	//private ArrayList<ConstantModel> data = new ArrayList<>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View v = inflater.inflate(R.layout.fragment_const, container, false);
		ButterKnife.bind(this, v);

		/*
		data.add(new ConstantModel("Pi", Math.PI));
		data.add(new ConstantModel("E", Math.E));
		data.add(new ConstantModel("The Answer", 42));


		for (int i = 0; i < 100; i++) {
			data.add(new ConstantModel(String.valueOf(i), i));
		}
		*/


		ConstantDatabase db = Room.databaseBuilder(
				getContext().getApplicationContext(),
				ConstantDatabase.class,
				"constants"
		).allowMainThreadQueries().build();
		db.constantDao().insert(
				new ConstantModel("Pi", Math.PI),
				new ConstantModel("E", Math.E),
				new ConstantModel("The Answer", 42)
		);

		ConstantAdapter adapter = new ConstantAdapter();
		list.setAdapter(adapter);
		list.setHasFixedSize(true);
		adapter.submitList(db.constantDao().getAll());
		db.close();

		return v;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof ConstListener) {
			mListener = (ConstListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement ConstListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface ConstListener {
		void showConstant(float constant);
	}
}
