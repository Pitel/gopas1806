package cz.gopas.kalkulacka;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(version = 1, entities = {ConstantModel.class})
public abstract class ConstantDatabase extends RoomDatabase {
	abstract public ConstantDao constantDao();
}
