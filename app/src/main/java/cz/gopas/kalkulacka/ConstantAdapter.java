package cz.gopas.kalkulacka;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ConstantAdapter extends ListAdapter<ConstantModel, ConstantAdapter.ConstantViewHolder> {

	public ConstantAdapter() {
		super(DIFF_CALLBACK);
	}

	@NonNull
	@Override
	public ConstantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new ConstantViewHolder(
				LayoutInflater.from(parent.getContext())
						.inflate(android.R.layout.simple_list_item_2, parent, false)
		);
	}

	@Override
	public void onBindViewHolder(@NonNull ConstantViewHolder holder, int position) {
		ConstantModel model = getItem(position);
		holder.text1.setText(model.name);
		holder.text2.setText(String.valueOf(model.value));
	}

	public static final DiffUtil.ItemCallback<ConstantModel> DIFF_CALLBACK =
			new DiffUtil.ItemCallback<ConstantModel>() {
				@Override
				public boolean areItemsTheSame(
						@NonNull ConstantModel oldContant, @NonNull ConstantModel newConstant) {
					// User properties may have changed if reloaded from the DB, but ID is fixed
					return oldContant.value == newConstant.value;
				}

				@Override
				public boolean areContentsTheSame(
						@NonNull ConstantModel oldConstant, @NonNull ConstantModel newConstant) {
					// NOTE: if you use equals, your object must properly override Object#equals()
					// Incorrectly returning false here will result in too many animations.
					return oldConstant.equals(newConstant);
				}
			};

	class ConstantViewHolder extends RecyclerView.ViewHolder {
		public TextView text1, text2;

		public ConstantViewHolder(View itemView) {
			super(itemView);
			text1 = itemView.findViewById(android.R.id.text1);
			text2 = itemView.findViewById(android.R.id.text2);
		}
	}
}
