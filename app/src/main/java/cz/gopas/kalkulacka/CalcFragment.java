package cz.gopas.kalkulacka;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AboutShower} interface
 * to handle interaction events.
 */
public class CalcFragment extends Fragment {

	private static final String TAG = CalcFragment.class.getSimpleName();

	@BindView(R.id.op)
	RadioGroup op;
	@BindView(R.id.res)
	TextView res;
	@BindView(R.id.a)
	EditText a;
	@BindView(R.id.b)
	EditText b;

	private SharedPreferences prefs;

	private AboutShower mListener;

	public CalcFragment() {
		// Required empty public constructor
	}


	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_calc, container, false);
		ButterKnife.bind(this, v);
		setHasOptionsMenu(true);
		prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
		return v;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.about:
				mListener.showAbout();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof AboutShower) {
			mListener = (AboutShower) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement AboutShower");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	@OnClick(R.id.calc)
	void calc() {
		Log.d(TAG, "Calc");

		float a = Float.parseFloat(this.a.getText().toString());
		float b = Float.parseFloat(this.b.getText().toString());

		float x;
		switch (op.getCheckedRadioButtonId()) {
			case R.id.add:
				x = a + b;
				break;
			case R.id.sub:
				x = a - b;
				break;
			case R.id.mul:
				x = a * b;
				break;
			case R.id.div:
				if (b == 0) {
					new AlertDialog.Builder(getContext())
							.setMessage(R.string.zerodiv)
							.show();
					x = Float.POSITIVE_INFINITY;
				} else {
					x = a / b;
				}
				break;
			default:
				x = Float.NaN;
		}

		res.setText(String.valueOf(x));
		prefs.edit().putFloat("RES", x).apply();
	}

	@OnClick(R.id.mem)
	void mem() {
		b.setText(String.valueOf(prefs.getFloat("RES", 0)));
	}

	@OnClick(R.id.constant)
	void showConstants() {
		mListener.showConstants();
	}

	@OnClick(R.id.share)
	void share() {
		Intent intent = new Intent(Intent.ACTION_SEND)
				.putExtra(Intent.EXTRA_TEXT, getString(R.string.result, res.getText()))
				.setType("text/plain");

		startActivity(intent);
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface AboutShower {
		void showAbout();
		void showConstants();
	}
}
