package cz.gopas.kalkulacka;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class ConstantModel {
	@PrimaryKey
	@NonNull
	public String name;
	public double value;

	public ConstantModel(String name, double value) {
		this.name = name;
		this.value = value;
	}
}
